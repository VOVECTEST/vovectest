package com.voverc.provisioning.overider;

import java.io.StringReader;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Properties;

public class DeskOverrider extends AbstractOverrider {

	@Override
	String getModelName() {
		return "DESK";
	}

	@Override
	boolean doOperation(HashMap<String, Object> configuration, String frame) {
		try {

			Properties property = new Properties();
			property.load(new StringReader(frame));
			Enumeration<?> propertyNames = property.propertyNames();

			while (propertyNames.hasMoreElements()) {
				String key = (String) propertyNames.nextElement();
				configuration.put(key, property.getProperty(key));
			}

		} catch (Exception e) {
			return false;
		}
		return true;
	}

}
