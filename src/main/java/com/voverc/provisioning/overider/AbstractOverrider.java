package com.voverc.provisioning.overider;

import java.util.HashMap;
import java.util.Objects;

public abstract class AbstractOverrider  {
	
	private static String KEY = "overrideFragment";
	
	/**
	 * Base method for overriding configuration
	 * @param configuration - map of key values
	 * @return
	 */
	public boolean override(HashMap<String,Object> configuration) {
	   	if (Objects.nonNull(configuration.get(KEY))) {
	   		if (configuration.get("model").toString().contains(getModelName())) {
	   			boolean result = doOperation(configuration, configuration.get(KEY).toString());
	   			if (result) {
	   				configuration.remove(KEY);
	   				return result;
	   			}
	   		}
	   	}
		return false;
	}
	
	/**
	 * Contains constant Models name
	 * @return
	 */
    abstract String getModelName();
    /**
     * Contain logic for overriding
     * @param configuration
     * @param frame - string with overriding information
     * @return
     */
    abstract boolean doOperation(HashMap<String,Object> configuration, String frame);
	
	
}
