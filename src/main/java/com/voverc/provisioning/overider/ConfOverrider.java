package com.voverc.provisioning.overider;

import java.util.HashMap;
import java.util.Map;

import com.google.gson.Gson;

public class ConfOverrider extends AbstractOverrider {

	@Override
	String getModelName() {
		return "CONFERENCE";
	}

	@Override
	boolean doOperation(HashMap<String, Object> configuration, String frame) {
		Gson gson = new Gson();
		try {
			Map<String, Object> map = gson.fromJson(frame, Map.class);
			if (map.isEmpty()) {
				return false;
			}
			for (Map.Entry<String, Object> item : map.entrySet()) {
				configuration.put(item.getKey(), item.getValue());
			}
		} catch (Exception e) {
			return false;
		}

		return true;
	}

}
