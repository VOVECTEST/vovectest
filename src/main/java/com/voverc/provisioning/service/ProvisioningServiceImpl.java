package com.voverc.provisioning.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.voverc.provisioning.configuration.ConfigurationCollector;
import com.voverc.provisioning.domain.Request;

@Service
public class ProvisioningServiceImpl implements ProvisioningService {
	
	@Autowired
	ConfigurationCollector configurationCollector;
	 
    public String getProvisioningFile(String macAddress) {
    	Gson gson = new Gson();
    	Request request = Request.newBuilder().setMac(macAddress).build();
    	Map<String, Object> config = configurationCollector.getConfig(request);
    	config.put("codecs", config.get("codecs").toString().split(","));
    	config.remove("model");
    	return gson.toJson(config);
    }
}
