package com.voverc.provisioning.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.voverc.provisioning.configuration.NotFound;
import com.voverc.provisioning.service.ProvisioningService;

@RestController
@RequestMapping("/api/v1")
public class ProvisioningController {

	@Autowired
	ProvisioningService service;
	
	@GetMapping(value = "provisioning/{mac}", produces = MediaType.APPLICATION_JSON_VALUE)
	public String getConfiguration(@PathVariable("mac") String macAddress) {
		return service.getProvisioningFile(macAddress);
	}
	
	@ExceptionHandler(NotFound.class)
	@ResponseStatus(code = HttpStatus.NOT_FOUND)
	public String handleException(NotFound e) {
	    return e.getMessage();
	}
}