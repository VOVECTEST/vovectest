package com.voverc.provisioning.repository;

import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.voverc.provisioning.configuration.ConfigurationElement;
import com.voverc.provisioning.domain.Request;
import com.voverc.provisioning.entity.Device;

@Component
public class DaoRepository implements ConfigurationElement {

	@Autowired
	DeviceRepository deviceRepository;

	@Override
	public boolean collect(Map<String, Object> map, Request request) {
		Optional<Device> device = deviceRepository.findById(request.getMac());
		if (device.isEmpty()) {
			return false;
		}
		map.put("model", device.get().getModel().toString());
		map.put("password", device.get().getPassword());
		map.put("username", device.get().getUsername());
		map.put("overrideFragment", device.get().getOverrideFragment());
		return true;
	}

}
