package com.voverc.provisioning.repository;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.AbstractEnvironment;
import org.springframework.core.env.Environment;
import org.springframework.core.env.MapPropertySource;
import org.springframework.core.env.PropertySource;
import org.springframework.stereotype.Repository;

import com.voverc.provisioning.configuration.ConfigurationElement;
import com.voverc.provisioning.domain.Request;

@Repository
public class PropertyRepository implements ConfigurationElement {

	@Autowired
	private Environment env;

	@Override
	public boolean collect(Map<String, Object> map, Request request) {
		boolean status = false;
		for (Iterator it = ((AbstractEnvironment) env).getPropertySources().iterator(); it.hasNext();) {
			PropertySource propertySource = (PropertySource) it.next();
			if (propertySource instanceof MapPropertySource) {
				Map<String, Object> source = ((MapPropertySource) propertySource).getSource();
				for (Map.Entry<String, Object> entry : source.entrySet()) {
					if (Objects.nonNull(entry.getKey())) {
						if (entry.getKey().contains("provisioning")) {
							map.put(entry.getKey().replace("provisioning.", ""), entry.getValue().toString());
							status = true;
						}
					}
				}
			}
		}
		return status;
	}

}
