package com.voverc.provisioning.domain;

import lombok.Data;

@Data
public class Request {

	private String mac;

	private Request() {}

	/**
	 *  Request build for default - create request
	 * @return
	 */
	public static Builder newBuilder() {
		return new Request().new Builder();
	}
	
	public class Builder {

		public Builder setMac(String mac) {
			Request.this.mac = mac;
			return this;
		}

		public Request build() {
			return Request.this;
		}

	}

}
