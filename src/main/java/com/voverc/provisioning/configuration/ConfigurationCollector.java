package com.voverc.provisioning.configuration;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.voverc.provisioning.domain.Request;
import com.voverc.provisioning.overider.AbstractOverrider;
import com.voverc.provisioning.overider.ConfOverrider;
import com.voverc.provisioning.overider.DeskOverrider;

@Component
public class ConfigurationCollector {

	@Resource(name="elementList") 
	List<ConfigurationElement> list;
	LinkedList<AbstractOverrider> overriders = new LinkedList<AbstractOverrider>() {
		{
			add(new DeskOverrider());
			add(new ConfOverrider());
		}
	};

	/**
	 * Base method for getting configuration
	 * @param request
	 * @return
	 * @throws NotFound
	 */
	public Map<String, Object> getConfig(Request request) throws NotFound {
		HashMap<String, Object> map = new HashMap<>();
		for (ConfigurationElement element : list) {
			boolean collect = element.collect(map, request);
			if (!collect) {
				throw new NotFound(String.format("Invalid mac %s",request.getMac()));
			}
		}

		for (AbstractOverrider overrider : overriders) {
			boolean result = overrider.override(map);
			if (result) {
				break;
			}
		}
		return map;

	}

}
