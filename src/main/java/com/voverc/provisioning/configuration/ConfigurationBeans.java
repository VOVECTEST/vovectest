package com.voverc.provisioning.configuration;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.voverc.provisioning.repository.DaoRepository;
import com.voverc.provisioning.repository.PropertyRepository;

@Configuration
public class ConfigurationBeans {

	@Autowired
	DaoRepository daoRepository;

	@Autowired
	PropertyRepository propertyRepository;

	@Bean
	public List<ConfigurationElement> elementList() {
		ArrayList<ConfigurationElement> list = new ArrayList<>();
		list.add(propertyRepository);
		list.add(daoRepository);
		return list;
	}

}
