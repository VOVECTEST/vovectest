package com.voverc.provisioning.configuration;

import java.util.Map;

import com.voverc.provisioning.domain.Request;

public interface ConfigurationElement {
	boolean  collect(Map<String, Object> map, Request request);
}
