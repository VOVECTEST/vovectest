package com.voverc.provisioning.configuration;

public class NotFound extends RuntimeException  {
	
	public NotFound(String message) {
		super(message);
	}

}
