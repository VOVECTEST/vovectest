package com.voverc.provisioning;

import java.util.HashMap;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.voverc.provisioning.entity.Device;
import com.voverc.provisioning.overider.ConfOverrider;
import com.voverc.provisioning.overider.DeskOverrider;

@SpringBootTest
public class OverrideTest {

	@Test
	void overriderConfTest() {
		ConfOverrider basic = new ConfOverrider();
		HashMap<String, Object> map = new HashMap<>();
		map.put("overrideFragment", "{\"domain\":\"sip.anotherdomain.com\",\"port\":\"5161\",\"timeout\":10}");
		map.put("model",Device.DeviceModel.CONFERENCE.toString());
		boolean override = basic.override(map);
		Assertions.assertTrue(override);
		Assertions.assertTrue(map.containsKey("timeout"));
		Assertions.assertFalse(map.containsKey("overrideFragment"));

	}

	@Test
	void overriderDescTest() {
		DeskOverrider str = new DeskOverrider();
		HashMap<String, Object> map = new HashMap<>();
		map.put("overrideFragment", "domain=sip.anotherdomain.com\nport=5161\ntimeout=10");
		map.put("model",Device.DeviceModel.DESK.toString());
		boolean override = str.override(map);
		Assertions.assertTrue(override);
		Assertions.assertTrue(map.containsKey("timeout"));
		Assertions.assertFalse(map.containsKey("overrideFragment"));

	}
}
