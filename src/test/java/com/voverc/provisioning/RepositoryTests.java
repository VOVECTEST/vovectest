package com.voverc.provisioning;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.voverc.provisioning.domain.Request;
import com.voverc.provisioning.domain.Request.Builder;
import com.voverc.provisioning.repository.DaoRepository;
import com.voverc.provisioning.repository.PropertyRepository;

@SpringBootTest
class RepositoryTests {

	@Autowired
	DaoRepository dao;

	@Autowired
	PropertyRepository repository;

	@Test
	void repositoryTest() {

		Builder request = Request.newBuilder().setMac("aa-bb-cc-dd-ee-ff");
		Map<String, Object> map = new HashMap<String, Object>();

		repository.collect(map, request.build());
		Assertions.assertFalse(map.isEmpty());

		map = new HashMap<String, Object>();
		dao.collect(map, request.build());
		Assertions.assertFalse(map.isEmpty());

	}

}
