package com.voverc.provisioning;

import java.util.Map;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.voverc.provisioning.configuration.ConfigurationCollector;
import com.voverc.provisioning.configuration.NotFound;
import com.voverc.provisioning.domain.Request;

@SpringBootTest
public class CollectorTest {

	@Autowired
	ConfigurationCollector collector;
	
	@Test
	void collectTest() {
		Request request = Request.newBuilder().setMac("a1-b2-c3-d4-e5-f6").build();
		try {
			Map<String, Object> config = collector.getConfig(request);
			Assertions.assertFalse(config.isEmpty());
			Assertions.assertTrue(config.containsKey("timeout"));
			Assertions.assertFalse(config.containsKey("overrideFragment"));
		} catch (NotFound e) {
			Assertions.assertTrue(false);
		}
	}
	
	
	@Test
	void collectNotFoundTest() {
		Request request = Request.newBuilder().setMac("a1-b2-c3-d4-e5-f61").build();
		try {
			Map<String, Object> config = collector.getConfig(request);
			Assertions.assertTrue(false);
		} catch (NotFound e) {
			Assertions.assertTrue(true);
		}
		

	}
}
